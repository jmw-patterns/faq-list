<?php
namespace Dudley\Patterns\Pattern\FAQList;

/**
 * Class ACFFAQList
 *
 * @package Dudley\Patterns\Pattern\FAQList
 */
class ACFFAQList extends FAQList {
	/**
	 * @var string
	 */
	public static $meta_type = 'acf';

	/**
	 * ACFFAQList constructor.
	 */
	public function __construct() {
		if ( ! get_field( 'faq_list' ) ) {
			return;
		}

		while ( has_sub_field( 'faq_list' ) ) {
			$this->add_item( new FAQListItem(
				get_sub_field( 'faq_list_item_question' ),
				get_sub_field( 'faq_list_item_answer' )
			) );
		}

		parent::__construct( $this->items );
	}
}
