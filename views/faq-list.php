<?php
/**
 * @var $module Dudley\Patterns\Pattern\FAQList\FAQList;
 * @var $item   Dudley\Patterns\Pattern\FAQList\FAQListItem;
 */
?>

<section class="faq-list">
	<ul class="faq-list__list">
		<?php foreach ( $module->get_items() as $item ) : ?>
			<li class="faq-list__item faq">
				<h6 class="faq__question"><?php $item->question(); ?></h6>

				<div class="faq__answer user-content">
					<?php $item->answer(); ?>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>
</section>
