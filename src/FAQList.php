<?php
namespace Dudley\Patterns\Pattern\FAQList;

use Dudley\Patterns\Abstracts\AbstractRepeater;

/**
 * Class FAQList
 * @package Dudley\Patterns\Pattern\PageBuilder\FAQsBlock
 */
class FAQList extends AbstractRepeater {
	/**
	 * @var string
	 */
	public static $action_name = 'faq_list';

	/**
	 * FAQList constructor.
	 *
	 * @param array $items
	 */
	public function __construct( array $items ) {
		$this->items = $items;
	}

	/**
	 * @return array
	 */
	public function requirements() {
		return [
			$this->items,
		];
	}
}
