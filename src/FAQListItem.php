<?php
namespace Dudley\Patterns\Pattern\FAQList;

use Dudley\Patterns\Abstracts\AbstractItem;

/**
 * Class FAQListItem
 *
 * @package Dudley\Patterns\Pattern\FAQList
 */
class FAQListItem extends AbstractItem {
	/**
	 * @var
	 */
	private $question;

	/**
	 * @var
	 */
	private $answer;

	/**
	 * FAQListItem constructor.
	 */
	public function __construct( $question, $answer ) {
		$this->question = $question;
		$this->answer   = $answer;
	}

	/**
	 * @return array
	 */
	public function requirements() {
		return [
			$this->question,
			$this->answer,
		];
	}

	/**
	 * Print a frequently asked question.
	 */
	public function question() {
		esc_html_e( $this->question );
	}

	/**
	 * Print the answer to a frequently asked question.
	 */
	public function answer() {
		echo wp_kses_post( $this->answer );
	}
}
